package com.example.myapplication.views

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.models.Repo


class RepositoryAdapter(private var list:List<Repo> = listOf()) : RecyclerView.Adapter<RepositoryAdapter.RepositoryVH>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepositoryVH {
        val v: View = LayoutInflater.from(parent.context).inflate(R.layout.item_repo, parent, false)
        return RepositoryVH(v)
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: RepositoryVH, position: Int) {
        holder.bindItem(list[position])
    }

    inner class RepositoryVH (itemView: View) : RecyclerView.ViewHolder(itemView){
        fun bindItem(repo : Repo){
            itemView.findViewById<TextView>(R.id.title).text = repo.name
        }
    }
}