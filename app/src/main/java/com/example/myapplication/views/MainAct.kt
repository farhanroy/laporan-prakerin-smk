package com.example.myapplication.views

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.models.Repo
import com.example.myapplication.services.GitHubService
import com.example.myapplication.services.NetworkService
import kotlinx.android.synthetic.main.act_main.*
import kotlinx.coroutines.*
import org.koin.android.ext.android.get

class MainAct : AppCompatActivity() {
    private lateinit var repoAdapter: RepositoryAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.act_main)

        // get class NetworkService with Koin DI
        val service: NetworkService = get()
        // get service
        val gitHubService: GitHubService = service.getGitHubService()
        // repository Name
        val call = gitHubService.listRepos("roy1441")

        CoroutineScope(Dispatchers.IO).launch {
            // COROUTINE SCOPE
            withContext(Dispatchers.IO) {
                try {
                    val response = call.await()
                    if (response.isSuccessful) {
                        // Get Responses List
                        val data: List<Repo>? = response.body()
                        // init Recycler View Adapter
                        runOnUiThread { if(data != null) initRepoList(data) }
                        // Log Has Data
                        Log.d("MainActicity", "Success ${data?.size} Repos Found")
                    } else {
                        // Log Error
                        Log.d("MainActicity", "Error : Status ${response.code()} ")
                    }
                } catch (e: Exception) {
                    Log.d("MainActicity", "Exception ${e.message}") // Log Error

                }
            }
        }
    }

    private fun initRepoList(list:List<Repo>){
        repoAdapter = RepositoryAdapter(list)
        rv.layoutManager = LinearLayoutManager(this)
        rv.adapter = repoAdapter
    }
}
