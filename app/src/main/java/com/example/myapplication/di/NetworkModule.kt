package com.example.myapplication.di

import com.example.myapplication.services.NetworkService
import org.koin.dsl.module

var networkModules = module {
    single { NetworkService() }
}